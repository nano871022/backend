

# Backend #

Este hace parte del curso de nodeJS con express y MongoDB que ofrece Coursea y hace parte del Curso FullStackWeb

Para descargar este proyecto por medio de consola se realiza con el siguiente comando
git clone https://nano871022@bitbucket.org/nano871022/backend.git

En cada una de las semanas se trabajara por ramas y cada rama su nombre se refiere con la semana en la cual se esta trabajando.
git branch Semana1
git switch Semana1


### NodeJS ###

### Express ###

Se realiza la instalacion con npm i express-generator -g
Se crea proyecto express red-bicicletas --view=pug

pug: Es un template con un motor muy confiable por Haml, es implementado con javascript para NodeJS y Navegadores, ayuda a generar template y procesar texto y transformar a html, ver mas sobre https://pugjs.org

   change directory:
     $ cd red-bicicletas

   install dependencies:
     $ npm install

   run the app:
     $ DEBUG=red-bicicletas:* npm start



### MongoDB ###
