var mongose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var api = require('../../controllers/api/bicicletaControllerApi');
var request = require('request');
var server = require('../../bin/www');

describe("bicicleta api  ",()=>{

    beforeAll((done)=>{
        console.log("creando conexxion");
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongose.connect(mongoDB,{useNewUrlParser: true});
        const db = mongose.connection;
        db.on('error', console.error.bind(console,"connection error "));
        db.once('open',function(){
            console.log("Conectados a bd");
            done();
        });
        console.log("creando conexxion end");
    });

    afterEach((done)=>{
        Bicicleta.deleteMany({}, (error,success)=>{
            if(error) console.log(error);
            done();
        });
    });


    describe('get bicicleta /',()=>{
        it("Status 200", ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var bici1 = new Bicicleta(1,"negro","urbana",[6.123,-57.564]);
            Bicicleta.add(bici1);

            request.get('http://localhost:3000/api/bicicletas', function (error, response,body){
                expect(response.statusCode).toBe(200);
            });
        });
    });
    describe("POST bicicletas /create",() =>{
        it("status 200",(done)=>{
            var headers = {'Content-Type':"application/json"};
            var bici = `{"color":"rojo","modelo":"montaña","latitud": "6.321","longitud": "-57.454"}`;
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: bici
            },function(error,response,body){
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("rojo");
                done();
            });
        });
    });

    describe("POST bicicletas /update",() =>{
        it("status 200",()=>{
            var headers = {'Content-Type':"application/json"};
            var bici = `{"id":1,"color":"red","modelo":"mountain","latitud":"6.231","longitud":"-57.156"}`;
            var bici1 = new Bicicleta(1,"Rojo","race",[6.741,-54.465]);
            Bicicleta.add(bici1);
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/update',
                body: bici
            },(error, response, body) => {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe("red");
                expect(Bicicleta.findById(1).modelo).toBe("mountain");
            });
        });
    });

    describe("GET bicicletas /delete/:id",()=>{
        it("status 200",()=>{
            var bici1 = new Bicicleta(1,"Rojo","race",[6.741,-54.465]);
            Bicicleta.add(bici1);
            var bici2 = new Bicicleta(2,"Azul","mountain",[6.741,-54.465]);
            Bicicleta.add(bici2);
            var bici3 = new Bicicleta(3,"Amarrilo","city",[6.741,-54.465]);
            Bicicleta.add(bici3);
            expect(Bicicleta.allBicis.length).toBe(3);
            request.get("http://localhost:3000/api/bicicletas/delete/2",(error, response, body) => {
                expect(Bicicleta.allBicis.length).toBe(2);
            });
        });
    });
});

