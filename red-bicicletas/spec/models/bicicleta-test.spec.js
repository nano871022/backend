var mongose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe("testing bicicletas",()=>{
    beforeAll((done)=>{
        console.log("creando conexxion");
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongose.connect(mongoDB,{useNewUrlParser: true});
        const db = mongose.connection;
        db.on('error', console.error.bind(console,"connection error "));
        db.once('open',function(){
            console.log("Conectados a bd");
            done();
        });
        console.log("creando conexxion end");
    });

    afterEach((done)=>{
        Bicicleta.deleteMany({}, (error,success)=>{
            if(error) console.log(error);
            done();
        });
    });

   
    describe("Bicicleta.createInstance",()=>{
        it("crea una instancia de bicicletas",()=>{
            var bici = Bicicleta.createInstance(1,"verde","urbana",[6.231,-57.654]);

            expect(bici.codigo).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(6.231);
            expect(bici.ubicacion[1]).toEqual(-57.654);
        });
    });

    describe("Bicicletas.allBicis",()=>{
        it("comienza vacia",(done)=>{
            Bicicleta.allBicis((error,bicis)=>{
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe("Bicicleta.add",() =>{
        it("Agregar una", () => {
            var codigo = 2;
            var color = "Rojo";
            var modelo = "mountain";
            var ubicacion = [6.123,-57.1564];
    
            var bici = new Bicicleta({codigo:codigo,color:color,modelo:modelo,ubicacion:ubicacion});
            Bicicleta.add(bici,(error,newBici)=>{
                if(error) console.log(error);
                Bicicleta.allBicis((error,bicis)=>{
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].codigo).toEqual(newBici.codigo);
                    expect(bicis[0].color).toEqual(newBici.color);
                    expect(bicis[0].modelo).toEqual(newBici.modelo);
                });
            });
        });
    });

    describe("Bicicleta.findByCodigo",()=>{
        it("debe devolver la bici con codigo 1",(done)=>{
            Bicicleta.allBicis((error,bicis)=>{
                expect(bicis.length).toBe(1);

                var aBici = new Bicicleta({codigo:1,color:"verde",modelo:"urbana"});
                Bicicleta.add(aBici,(error,newBici)=>{
                    if(error)console.log(error);

                    var aBici2 = new Bicicleta({codigo:2,color:"rojo",modelo:"urbana"});
                    Bicicleta.add(aBici2,(error2,newBici2)=>{
                        if(error2) console.log(error2);
                        Bicicleta.findByCodigo(1,(error,biciFound)=>{
                            expect(biciFound.codigo).toBe(1);
                            expect(biciFound.color).toBe(aBici.color);
                            expect(biciFound.modelo).toBe(aBici.modelo);
                            done();
                        });
                    });
                });

            });
        });
    });

    describe("Bicicleta.removeByCodigo",()=>{
        it("debe eliminar la bici con codigo 1",(done)=>{
            Bicicleta.allBicis((error,bicis)=>{
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({codigo:1,color:"verde",modelo:"urbana"});
                Bicicleta.add(aBici,(error,newBici)=>{
                    if(error)console.log(error);

                    var aBici2 = new Bicicleta({codigo:2,color:"rojo",modelo:"urbana"});
                    Bicicleta.add(aBici2,(error2,newBici2)=>{
                        if(error2) console.log(error2);
                        Bicicleta.removeByCodigo(1,(error3)=>{
                            if(error3) console.log(error3);
                            done();
                        });
                    });
                });

            });
        });
    });
    

});