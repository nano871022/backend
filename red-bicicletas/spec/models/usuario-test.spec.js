var mongose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');

describe("realizando conecion con reservas ",()=>{

    beforeAll((done)=>{
        console.log("creando conexxion");
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongose.connect(mongoDB,{useNewUrlParser: true});
        const db = mongose.connection;
        db.on('error', console.error.bind(console,"connection error "));
        db.once('open',function(){
            console.log("Conectados a bd");
            done();
        });
        console.log("creando conexxion end");
    });


    afterEach((done)=>{
        Reserva.deleteMany({},(error3,success)=>{
            if(error3) console.log(error3);
            Usuario.deleteMany({},(error2, success)=>{
                if(error2) console.log(error2);
                Bicicleta.deleteMany({}, (error,success)=>{
                    if(error) console.log(error);
                    done();
                });
            });
        });
    });


    describe("Cuando un Usuario reserva una bici",() =>{
        it("debe existir la reserva",(done)=>{
            const usuario = new Usuario({nombre: "Alejandro"}); 
            usuario.save();
            const bicicleta = new Bicicleta({codigo: 1, color: "verde", modelo: "urbana"});
            bicicleta.save();

            var hoy = new Date();
            var tomorrow = new Date();
            tomorrow.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, tomorrow,(error, reserva)=>{
                Reserva.find({}).populate('bicicletas').populate('usuario').exec((error, reservas)=>{
                    console.log(reservas[0]);
                    console.log()
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicletas.codigo).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});