var map = L.map("main_map").setView([6.217,-75.567],12);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([6.218,-75.567]).addTo(map);
L.marker([6.217,-75.587]).addTo(map);
L.marker([6.227,-75.559]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result){
        console.log(result);
        result.bicicletas.forEach(function (bici){
            L.marker(bici.ubicacion,{title: bici.code}).addTo(map);
        });
    }
});