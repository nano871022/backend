const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_CLIENT_ID,
    clientSecret: process.env.FACEBOOK_CLIENT_SECRET
},(accessToken, refreshToken, profile, done)=>{
    try{
        Usuario.findOneOrCreateByFacebook(profile, (error, usuario)=>{
            if(error) console.log("error "+error);
            return done(error, usuario);
        });
    }catch(exception){
        console.log(exception);
        return done(exception,null);
    }
}));


passport.use(new localStrategy({
    usernameField: 'email',
    passwordField: 'password'
},(username, password, done)=>{
   // console.log("find: "+username);
    Usuario.findOne({email: username},(error,usuario)=>{
        //console.log("found "+username);
        if (error) return done(error);
        if (!usuario) return done(null,false,{message:"Email no existe o incorrecto"});
        if (!usuario.validPassword(password)) return done(null,false,{message: "Contraseña incorrecta."});
        if (!usuario.verificado) return done(null, false, {message: "El usuario no se encuentra verificado"});
        return done(null,usuario);
    });
}));

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
},(accessToken, refreshToken, profile, callback)=>{
    console.log(profile);
    Usuario.findOneOrCreateByGoogle(profile, (error, usuario)=>{
        return callback(error, usuario);
    });
}));

passport.serializeUser((usuario,callback)=>{
    console.log("serializeuser Usuario:"+usuario);
    console.dir(callback);
    Usuario.findById(usuario._id,(error,usuario)=>{
       // console.log("Encontrado...");
        //console.dir(error);
        //console.dir(usuario);
        callback(error,usuario);
    });
});

passport.deserializeUser((usuario, done)=>{
    //console.log(usuario);
    //console.dir(done);
    done(null,usuario);
});

module.exports = passport;