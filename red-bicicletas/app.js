require('dotenv').config();
var createError         = require('http-errors');
var express             = require('express');
var path                = require('path');
var cookieParser        = require('cookie-parser');
var logger              = require('morgan');
const passport          = require('./config/passport');
const session           = require('express-session');
var indexRouter         = require('./routes/index');
var usersRouter         = require('./routes/users');
var tokenRouter         = require('./routes/token');
var recoveryRouter      = require('./routes/recovery');
var bicicletasRouter    = require('./routes/bicicletas');
var loginRouter         = require('./routes/login');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuariosApiRouter   = require('./routes/api/usuarios');
var authApiRouter       = require('./routes/api/authenticate');
var db                  = require('./business/mongose.business');
const { assert } = require('console');
const MongoDBStore      = require('connect-mongodb-session')(session);

let store;
if(process.env.NODE_ENV === 'development'){
  const store = new session.MemoryStore;
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error',(error)=>{
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();

app.use(session({
  cookie: {maxAge: 1000*60*60*24*10},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicicletas!*****![][]!!123'
}));

app.set('secretkey', 'jwt_pwd_!asda123123');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use("/privacy_policy.html", (request, response)=>{
  response.sendFile("public/privacy_policy.html");
});

app.use("/google343727a350acd05f.html", (request, response)=>{
  response.sendFile("public/google343727a350acd05f.html");
});

app.get('/auth/google',passport.authenticate('google',{scope:[
  'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/plus.profile.emails.read'
]}));

app.get('/auth/undefined/auth/google/callback',passport.authenticate('google',{
  successRedirect: '/',
  failureRedirect: '/error'
}));


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/token', tokenRouter);
app.use('/login', loginRouter);
app.use('/recovery', recoveryRouter);
app.use('/bicicletas',loggedIn,bicicletasRouter);
app.use('/api/bicicletas',bicicletasApiRouter);
app.use('/api/usuarios',usuariosApiRouter);
app.use('/api/auth', authApiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(request, response, next){
  if(request.user){
    next();
  }else{
    console.log('User sin loguearse');
    response.redirect('/login');
  }
};

module.exports = app;
