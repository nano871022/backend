var Bicicleta = require('../models/bicicleta');
const router = require('../routes');
const { response } = require('express');

exports.bicicleta_list = function (request,response){
    Bicicleta.allBicis((error, bicis)=>{
        if(error) response.status(200).send({msg:'Error no se encontraron bicicletas '+error});
        console.dir("Valores:: ",bicis);
        response.render('bicicletas/index',{bicis: bicis});
    });
}

exports.bicicleta_create_get = function (request, response){
    response.render('bicicletas/create');
}

exports.bicicleta_create_post = function (request, response){
    var ubicacion = [request.body.latitud,request.body.longitud];
    var bici = new Bicicleta({
        codigo: request.body.id,
        color: request.body.color,
        modelo: request.body.modelo,
        ubicacion: ubicacion
    });
    Bicicleta.add(bici);

    response.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function ( request, response){
    Bicicleta.removeById(request.body.id);

    response.redirect("/bicicletas");
}

exports.bicicleta_update_get = function (request, response){
    console.log("id: "+request.params.id);
    Bicicleta.findById(request.params.id,(error,bici)=>{
        if(error) response.status(200).send({msg:"Se presento un error "+error});
        console.dir("Encontrado",bici);
        response.render('bicicletas/update',{bici});
    });
}

exports.bicicleta_update_post = function (request, response){
    var bici = Bicicleta.findById(request.body.id);
    bici.ubicacion = [request.body.latitud,request.body.longitud];
    bici.color = request.body.color;
    bici.modelo = request.body.modelo;
    response.redirect('/bicicletas');    
}