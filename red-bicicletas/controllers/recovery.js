var Usuario = require('../models/usuario');
var Token = require('../models/token');
const { response } = require('express');

module.exports = {
    recovery_get: (request, response, next)=>{
        Token.findOne({
            token: request.params.token,
            recovery: true
             }, (error, token)=>{
                 Usuario.findById(token._usuarioId,(error,usuario)=>{
                    response.render('usuarios/recovery',{token:token.token,usuario: usuario,errors:{}});
                 });
        });
    },
    recovery: (request, response, next) =>{
       Token.findOne({
           token: request.params.token,
           recovery: true
        }, (error, token)=>{
            Usuario.findById(token._usuarioId,(error,usuario)=>{
                if(!usuario) return response.status(400).send({msg:"El usuario no fue encontrado con el token suministrado para poder realizar la recuperacion de contraseña."});
                token.recovery = false;
                token.save((error)=>{
                    if( error ) return response.status(500).send({msg:"No se logro actualizar el flag del token."});
                });
                if ( request.body.password != request.body.confirm_password ){
                    response.render('usuarios/recovery',{errors:{
                        confirm_password: 'Las contraseñas no coinciden.'
                    }});
                }
                usuario.save((error)=>{
                    if(error) return response.status(400).send({msg: 'No se logro actualizar la contraseña.'});
                    response.redirect('/users');
                });
            });
       }); 
    },
    generate: (request, response, next)=>{
        console.log("id "+request.params.id);
        Usuario.findOne({_id: request.params.id},(error,usuario)=>{
            if(error) return response.status(400).send({msg:'No se logro encontrar el usuario para generar el recovery.'});
            if(!usuario) return response.status(400).send({msg: 'No se logro encontrar el usuario '+error});
            usuario.generate_recovery(()=>{
                response.render("session/forgot",{});
            });
        });
    }
};