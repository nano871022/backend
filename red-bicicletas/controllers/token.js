var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
    confirmationGet: function(request, response, next){
        Token.findOne({token: request.params.token}, function(error, token){
            if( !token) return response.status(400).send({type: 'not-verified', msg: 'No se encontro un usuario con exte token.'});
            Usuario.findById(token._usuarioId,function (error, usuario){
                if (!usuario) return response.status(400).send({msg: 'No encontramos un usuario con este token'});
                if (usuario.verificado) return response.redirect('/users');
                usuario.verificado = true;
                
                usuario.save(function (error){
                    if(error) return response.status(500).send({msg: error.message});
                    response.redirect('/');
                });
            });
        });
    },
}