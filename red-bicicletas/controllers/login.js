const router = require('../routes');
const { response } = require('express');
const passport = require('../config/passport');
const Usuario = require('../models/usuario');

module.exports = {
    login_get : function(request,response){
        response.render('session/login');
    },
    login_post: function(request,response, next){
        passport.authenticate('local',(error,usuario,info)=>{
            console.dir(info);
            if(error) return next(error);
            if(!usuario) return response.render('session/login',{info});
            request.logIn(usuario,(error)=>{
                if (error) return next(error);
                return response.redirect("/");
            });
        })(request,response, next);
    },
    logout: function (request, response){
        request.logout();
        response.redirect("/");
    },
    forgotPassword_get: function(request, response){
        response.render('session/resetPassword');
    },
    forgotPassword_post: function(request, response,next){
        var email = request.body.email;
        console.log("Email:: "+email)
        Usuario.findOne({email:email},(error, usuario)=>{
            if(error) return response.status(400).send({msg:"No se encontro el usuario."});
            console.log(usuario.email);
            response.redirect('/recovery/'+usuario._id+'/generate');
        });
    }
};