var Usuario = require('../models/usuario');
const { model } = require('../models/usuario');

module.exports = {
    list: function (request, response, next){
        Usuario.find({},(error,usuarios) =>{
            response.render('usuarios/index',{usuarios: usuarios});
        });
    },
    update_get: function(request, response, next){
        Usuario.findById(request.params.id, function(error, usuario){
            response.render('usuarios/update', {errors: {}, usuario: usuario});
        });
    },
    update: function(request, response, next){
        var update_values = {nombre: request.body.nombre};
        Usuario.findByIdAndUpdate(request.param.id, update_values, function (error,usuario){
            if(error){
                console.log(error);
                response.render('usuarios/update',{errors: error.errors, usuario: new Usuario({nombre: request.body.nombre, email: request.body.email})});
            }else{
                response.redirect('/users');
                return;
            }
        });
    },
    create_get: function (request, response, next){
        response.render('usuarios/create',{errors:{}, usuario: new Usuario()});
    },
    create: function (request, response, next){
        if(request.body.password != request.body.confirm_password){
            response.render('usuarios/create',{
                    errors:{
                        confirm_password:{
                                message: 'No coincide con el passowd ingresado'
                            }
                        }, 
                        usuario: new Usuario({nombre: request.body.nombre, 
                                             email: request.body.email})
                    });
            return;
        }

        Usuario.create({nombre: request.body.nombre, email: request.body.email, password: request.body.password},function(error, usuario){
            if(error){
                response.render('usuarios/create',{
                                                    errors: error.errors, 
                                                    usuario: new Usuario({
                                                        nombre: request.body.nombre, 
                                                        email: request.body.email
                                                    })
                                                });
            }else{
                usuario.enviar_email_bienvenida();
                response.redirect('/users');
            }
        });
    },
    delete: function (request, response, next){
        Usuario.findByIdAndDelete(request.body.id, function (error){
            if(error){
                next(error);
            }else{
                response.redirect('/users');
            }
        });
    }
}