var Usuario = require('../../models/usuario');

exports.usuarios_list = function(request, response){
    Usuario.find({},function(error,usuarios){
        response.status(200).json({
            usuarios: usuarios
        });
    });
};

exports.usuarios_create = function(request, response){
    var usuario = new Usuario({nombre: request.body.nombre});
    usuario.save((error)=>{
        response.status(200).json(usuario);
    });
};

exports.usuarios_reservar = function(request, response){
    Usuario.findById(request.body.id,function(error, usuario){
        console.log(usuario);
        usuario.reservar(request.body.bici_id, request.body.desde, request.body.hasta,function(error){
            console.log("reserva !!!");
            response.status(200).send();
        });
    });
};