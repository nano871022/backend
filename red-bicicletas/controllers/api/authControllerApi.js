const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Usuario = require('../../models/usuario');


module.exports ={
    authenticate: function (request, response, next){
        Usuario.findOne({email: request.body.email},(error,usuario)=>{
            if(error) {
                next(error);   
            }else{
                if(usuario === null) return response.status(401).json({status:"error",message:"Usuario/password invalido", data:null});
                if(usuario != null && bcrypt.compareSync(request.body.password, usuario.password)){
                    const token = jwt.sign({id: usuario._id}, request.app.get('secretkey'),{ expiresIn: '7d'});
                    response.status(200).json({message: "usuario encontrado!", data:{usuario: usuario, token: token}});
                }else{
                    response.status(401).json({status:"error",messager: "Usuario/password es invalido",data:null})
                }
            }
        });
    },
    forgotPassword: function(request, response, next){
        usuario.findOne({email: request.body.email},(error, usuario)=>{
            if(!usuario) return response.status(401).json({message:"No existe el usuario"});
            usuario.generate_recovery(()=>{
                response.status(200).json({message:"Se envio un correo para la recuperacion del password"});
            });
        });
    },
    authFacebookToken: function(request, response, next){
        if(request.usuario){
            request.usuario.save().then(()=>{
                const token = jwt.sign({id: request.usuario.id}, request.app.get('secretkey'),{expiresIn: '7d'});
                response.status(200).json({message: "Usuario encotnrado o creado",data:{usuario: request.usuario, token: token}});
            }).catch((error)=>{
                console.log(error);
                response.status(500).json({message: error.message});
            });
        }else{
         response.status(401);
        }
    }
}