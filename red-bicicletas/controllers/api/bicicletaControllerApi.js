var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (request, response){
    Bicicleta.allBicis((error,bicis)=>{
        response.status(200).json({bicicletas: bicis});
    });
}

exports.bicicleta_create = function (request, response){
    var ubicacion = [request.body.latitud,request.body.longitud];
    Bicicleta.allBicis((error,bicis)=>{
        if(error)response.status(500).send({msg:error});
        console.log(bicis);
        var bici = new Bicicleta({
                codigo:bicis.length+1,
                color:request.body.color,
                modelo:request.body.modelo,
                ubicacion:ubicacion
            });
        Bicicleta.add(bici);
        response.status(200).json({status:'OK',bicicleta: bici})
    });
}

exports.bicicleta_update = function (request, response){
    var ubicacion = [request.body.latitud,request.body.longitud];
    var bici = Bicicleta.findById(request.body.id);
    bici.ubicacion = ubicacion;
    bici.color = request.body.color;
    bici.modelo = request.body.modelo;

    response.status(200).json({status:"ok", bicicleta: bici});
}

exports.bicicleta_delete = function (request, response){
    Bicicleta.removeById(request.params.id);
    response.status(200).json({status:"ok"});
}