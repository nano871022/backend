Para realizar la ejecucion de este projecto se realiza con 

Ejecucion en produccion
npm start

Para inicializar jasmine

node node_modules/jasmine/bin/jasmine init

Para cargar mongoDB
sudo systemctl start mongod


Ejecucion en desarrollo
npm run devstart ## configuracion realizada sobre package.json


Para cargar en HEROKU el proyecto que contiene una subcarpeta que es red-bicicletas se realiza lo siguiente:
sudo git subtree split --prefix red-bicicletas ## esto genera un token de respuesta
sudo git push heroku token:/refs/heads/master --force ## carga el codigo y despliega


las paginas son
http://127.0.0.1:3000/      ## index -  principal
http://127.0.0.1:3000/users ## usuarios


END point 
GET http://localhost:3000/api/bicicletas
response{
    bicicletas:[
        bicicleta:{
        "color": "valor",
        "modelo": "valor",
        "longitud": valor,
        "latitud": valor
        "id":valor
    }]
}
POST http://localhost:3000/api/bicicletas/create
 request{
     "color": "valor",
     "modelo": "valor",
     "longitud": valor,
     "latitud": valor
 }
 response{
     status: "OK",
     bicicleta:{
        "color": "valor",
        "modelo": "valor",
        "longitud": valor,
        "latitud": valor
        "id":valor
    }
 }
POST http://localhost:3000/api/bicicletas/update
request{
     "color": "valor",
     "modelo": "valor",
     "longitud": valor,
     "latitud": valor,
     "id": valor
 }
response{
     status: "OK",
     bicicleta:{
        "color": "valor",
        "modelo": "valor",
        "longitud": valor,
        "latitud": valor
        "id":valor
    }
 }
 GET http://localhost:3000/api/bicicletas/delete/:id
 response{
     status: "OK"
 }
