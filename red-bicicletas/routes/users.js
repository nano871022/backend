var express = require('express');
var usuariosController = require('../controllers/usuarios');
var router = express.Router();

/* GET users listing. */
router.post('/:id/update', usuariosController.update);
router.get('/:id/update', usuariosController.update_get);
router.post('/:id/delete', usuariosController.delete);
router.post('/create', usuariosController.create);
router.get('/', usuariosController.list);
router.get('/create', usuariosController.create_get);

module.exports = router;
