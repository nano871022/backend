var express    = require('express');
var router     = express.Router();
const passport = require('../config/passport');
const login    = require('../controllers/login');

router.get ("/",login.login_get);
router.post('/',login.login_post);
router.get ("/logout",login.logout);
router.get ("/forgotPassword",login.forgotPassword_get);
router.post("/forgotPassword",login.forgotPassword_post);

module.exports = router;