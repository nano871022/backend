var express = require('express');
var recoveryController = require('../controllers/recovery');
var router = express.Router();

/* GET users listing. */
router.get('/:token/recovery/', recoveryController.recovery_get);
router.post('/:token/recovery', recoveryController.recovery);
router.get('/:id/generate/', recoveryController.generate);

module.exports = router;
