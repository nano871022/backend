var express = require('express');
const { request } = require('express');
var router = express.Router();
var auth = require('../../controllers/api/authControllerApi');
const passport = require('passport');

router.post("/authenticate",auth.authenticate);
router.post("/resetpassword",auth.forgotPassword);
router.post("/facebook_token",passport.authenticate('facebook-token'),auth.authFacebookToken);

module.exports = router;