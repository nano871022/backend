var express = require('express');
var validarUsuario = require('../../models/seguridad');
var router = express.Router();
var bicicletaControllerApi = require('../../controllers/api/bicicletaControllerApi');

router.get("/",validarUsuario,bicicletaControllerApi.bicicleta_list);
router.post("/create",bicicletaControllerApi.bicicleta_create);

router.get("/delete/:id",bicicletaControllerApi.bicicleta_delete);
router.post("/update",bicicletaControllerApi.bicicleta_update);
//router.post("/:id/update",bicicletaControllerApi.bicicleta_update_post);

module.exports = router;