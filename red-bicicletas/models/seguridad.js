var jwt = require('jsonwebtoken');

function validarUsuario(request, response, next){
    jwt.verify(request.headers['x-access-token'],request.app.get('secretkey'),(error, decoded)=>{
        if(error){
            response.json({status:"error", message: error.message, data:null});
        }else{
            request.body.userId = decoded.id;
            console.log('jwt verify: '+decoded);
            next();
        }
    });
}

module.exports = validarUsuario;