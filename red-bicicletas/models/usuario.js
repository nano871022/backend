var mongose = require('mongoose');
var crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
var bcrypt = require('bcrypt');
var Schema = mongose.Schema;
var Reserva = require('./reserva');
var Token = require('./token');
const mailer = require('../mailer/mailer');
const { getMaxListeners } = require('./reserva');

const validateEmail = function (email){
    const regi = /^\w+([\.~]?\w+)+@\w+(\.\w{2,3})+$/;
    return regi.test(email);
};
const saltOrRound = 10;

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        require: [true, 'El email es obligatorio'],
        lowercase: true,
        validate: [validateEmail, 'Por favor, ingrese un email valido'],
        match: [/^\w+([\.~]?\w+)+@\w+(\.\w{2,3})+$/],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator,{message: 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password,saltOrRound);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password,this.password);
}

usuarioSchema.methods.reservar = function (bici, desde, hasta, callback){
    var reserva = new Reserva({
        usuario: this._id,
        bicicletas: bici,
        desde: desde,
        hasta: hasta
    });
    console.log(reserva);
    reserva.save(callback);
};

usuarioSchema.methods.enviar_email_bienvenida = function(callback){
    const token = new Token({_usuarioId: this._id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (error){
        if(error) return console.log(error.message);

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'verificacion de cuenta',
            text: 'Hola,\n\n por favor verificar su cuenta haga click en este link: \n http://localhost:3000/token/confirmation/'+token.token+' \n'
        };

        mailer.sendMail(mailOptions, function(error){
            if(error) return console.log(error.message);
            console.log('A verification email has been sent to '+email_destination+'.');        
        });
    });
};

usuarioSchema.methods.generate_recovery = function(callback){
    const token =new Token({recovery:true,_usuarioId: this._id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save((error)=>{
        if(error) return console.log(error,message);

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Recuperar contraseña',
            text: 'Hola\n\n para realizar la recuperacion de la contraseña realizar click sobre el link http://localhost:3000/recovery/'+token.token+'/recovery \n'
        };
        
        mailer.sendMail(mailOptions, (error)=>{
            if(error) return console.log(error.message);
            console.log('A recovery password has been sent to '+email_destination+'.');
        });
        callback();
    });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'googleId': condition.id},
            {'email': condition.emails[0].value}
        ]},(error, result)=>{
            if(result){
                callback(error, result);
            }else{
                console.log('------------ CONDICION -----------');
                console.log(condition);
                let values = {};
                values.googleId   = condition.id;
                values.email      = condition.emails[0].value;
                values.nombre     = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password   = condition._json.etag;
                console.log('------------ VALUES -------------');
                console.log(values);
                self.create(values, (error, result)=> {
                    if(error){ console.log(error);}
                    return callback(error, result);
                });
            }
    });
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'facebookId': condition.id},
            {'email': condition.emails[0].value}
        ]},(error, result)=>{
            if(result){
                callback(error, result);
            }else{
                console.log('------------ CONDICION -----------');
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email      = condition.emails[0].value;
                values.nombre     = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password   = crypto.randomBytes(16).toString('hex');
                console.log('------------ VALUES -------------');
                console.log(values);
                self.create(values, (error, result)=> {
                    if(error){ console.log(error);}
                    return callback(error, result);
                });
            }
    });
}

module.exports = mongose.model("Usuario",usuarioSchema);