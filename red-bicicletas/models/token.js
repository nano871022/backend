const mongose = require('mongoose');

const Schema = mongose.Schema;

const TokenSchema = new Schema({
    _usuarioId: {
        type: mongose.Schema.Types.ObjectId,
        required: true,
        ref: 'Usuario'
    },
    token: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 43200
    },
    recovery:false
});
module.exports = mongose.model('Token',TokenSchema);