var mongose = require('mongoose');
var Schema = mongose.Schema;

var bicicletaSchema = new Schema({
    codigo: Number,
    color: String,
    modelo: String,
    ubicacion:{
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true
        }
    }
});

bicicletaSchema.methods.toString = function(){
    return 'code: '+ this.codigo+' | color: '+this.color; 
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({},cb);
};

bicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
    return new   this({
        codigo: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });  
};

bicicletaSchema.statics.add = function (bici, callback){
    this.create(bici,callback);
};

bicicletaSchema.statics.findByCodigo = function(codigo, callback){
    return this.findOne({codigo: codigo},callback);
};

bicicletaSchema.statics.removeByCodigo = function(codigo,callback){
    return this.deleteOne({codigo: codigo},callback);
};

module.exports = mongose.model("Bicicleta", bicicletaSchema);